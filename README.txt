The "Node" Concept.

"Node", has a very specific, but different, meaning in both in the Neo4j and
Drupal world. In the module, every attempt has been made to use the "entity"
term when refering to Drupal concepts, and leaving "Neo nodes" as a Neo4j
specific concept.

However, given the history of the node word in Drupal terms, it's possible
this hasn't been correctly implemented everywhere. File a bug if you find any
inconsistencies.
